<?php session_start();
require('../dbconnect.php');

$_SESSION['cate_id'] = $_REQUEST['id'];

// カテゴリ名を取得
$cate3 = $_SESSION['cate_id'];

$categories = $db->prepare('SELECT question_name FROM categories WHERE id = :cate');
$categories->bindParam(':cate',$cate3);
$categories->execute();
$cate = ($categories->fetchAll());

?>
<!doctype html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="../css/users/input.css" >
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Exercise</title>
  </head>
  <body>
    <main>
      <h4 class="text-center mt-5"><?php echo $cate[0]['question_name'] ?></h4>
      <div class="text-center">(全3問)</div>
      <form class="container mt-5 px-5 text-center" action = "../quizzes/index.php?id=<?php echo ($_SESSION['cate_id']); ?>" method = "post">
        <div class="form-group">
          <input type="text" name="name" class="form-control form-control-sm"  placeholder="回答者名を入力してください。" />
        </div>
        <button type="submit" class="btn btn-secondary mt-4">テスト開始</button>
      </form>
    </main>
  </body>    
</html>