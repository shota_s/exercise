<?php
  session_start();
  require('../dbconnect.php');

 
// 回答者をセッションに
  if (isset($_POST['name'])) {
    $_SESSION['name'] = $_POST['name'];
  }

// 回答者登録
$username = $db->prepare('INSERT INTO users SET name=?, created_at=NOW()');
$username->execute(array($_POST['name']));
  
// 問題を取得
$cate = $_SESSION['cate_id'];

$questions = $db->prepare('SELECT DISTINCT question, id FROM quizzes WHERE quizzes.category_id = :cate');
$questions->bindParam(':cate',$cate);
$questions->execute();
  
while($row2 = $questions->fetch(PDO::FETCH_ASSOC)){  
  $ques[]=array(
    'quizzes' => array('question' => $row2['question'],'id' => $row2['id'])  
  );
}


$ca = count($ques)-1;

  if(!isset($rand)){
    $rand=range(0,$ca);
    shuffle($rand);
    for($i=1;$i<=$ca;$i++){
      $mondai[$i]=$rand[$i];
    }
  }


  $rcode = $mondai[1];
  $_SESSION['qcode'] = $ques[$rcode]['quizzes']['id'];

  // 問題を生成
  $stmt=$db->prepare("SELECT choices.id, choices.quiz_id, choices.correct_answer, choices.choice, quizzes.category_id FROM choices INNER JOIN quizzes ON choices.quiz_id = quizzes.id WHERE choices.quiz_id = :qcode;");
  $stmt->bindParam(':qcode',$ques[$rcode]['quizzes']['id']);
  $stmt->execute();
  $_SESSION['code']=$code;

  $answers = array();
  while($row = $stmt->fetch(PDO::FETCH_ASSOC)){  
    $answers[]=array(
    'choices' => array('id' => $row['id'], 'choice' => $row['choice'], 'quiz_id' => $row['quiz_id'], 'correct_answer' => $row['correct_answer']),
    'quizzes' => array('category_id' => $row['category_id'], 'num' => $row['num'])
    );
  }

  // 第何問表示
  $monme2 = $_POST['next_num'];
  if (!isset($_POST['next_num'])){
    $monme = 1;
  }else{
    $monme = $monme2+1;
  }

  // カテゴリ名を取得
$cate3 = $_SESSION['cate_id'];

$categories = $db->prepare('SELECT question_name FROM categories WHERE id = :cate');
$categories->bindParam(':cate',$cate3);
$categories->execute();
$cate = ($categories->fetchAll());

?>
<!doctype html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="../css/quizzes/_index.css" >
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
   
    <title>Exercise</title>
  </head>
  <body>
    <main>
      <div class="container">
        <h4 class="text-center mt-5"><?php echo $cate[0]['question_name'] ?></h4>
          <div class="text-center">(全3問)</div>
          <div class="center-block ques-num">
            問題 <?php echo $monme; ?>
          </div>
          <div class="center-block ques">
            <?php      
              echo $ques[$rcode]['quizzes']['question'];
            ?>
          </div>
          <div class="center-block ques">
            <form method="post" action="quiz_result.php"> 
              <div class="form-group mt-4">
                <div class="form-check">
                  <?php $c=1; foreach ($answers as $options) : ?>
                    <label>
                    <input class="form-check-input" type="radio" name="kotae" id="kotae" value="<?php echo $options['choices']['id']?>">
                    <?php echo $c++ ?>. <?php echo $options['choices']['choice'] ?>
                    </label>
                    <br/>
                    <input type="hidden" name="correct" value="<?php echo $options['choices']['correct_answer'] ?>">
                    <input type="hidden" name="ques_num" value="<?php echo $monme ?>">
                  <?php endforeach; ?>                
                </div>
                <input class="text-center mt-5" type="submit" value="回答">
              </div>
            </form>
          </div>
      </div>
    </main>
  </body>    
</html>