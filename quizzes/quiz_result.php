<?php
  session_start();
  require('../dbconnect.php');

  // 選択肢取得
  $stmt=$db->prepare("SELECT choices.id, choices.quiz_id, choices.correct_answer, choices.choice FROM choices INNER JOIN quizzes ON choices.quiz_id = quizzes.id WHERE choices.quiz_id = :code;");
  $stmt->bindParam(':code',$code);
  $stmt->execute();

  $answers = array();
  while($row = $stmt->fetch(PDO::FETCH_ASSOC)){  
    $answers[]=array(
    'choices' => array('id' => $row['id'], 'choice' => $row['choice'], 'quiz_id' => $row['quiz_id'], 'correct_answer' => $row['correct_answer']),
    'quizzes' => array('category_id' => $row['category_id'], 'num' => $row['num'])
    );
  }
    
  // 問題取得
  $questions = $db->prepare('SELECT DISTINCT question, id FROM quizzes WHERE id=:code;');
  $questions->bindParam(':code',$code);
  $questions->execute();
  $question = $questions->fetch();
  $ques = array_unique($question);

  // 正誤判定
  $ans = $_POST['kotae'];
  $correct = $_POST['correct'];

  if($ans == $correct){
    $hantei="正解!";
    if(isset($_SESSION['ok_count'])){
      $ok_count=$_SESSION['ok_count']+1;
    }else{
      $ok_count=1;
    }
    $_SESSION['ok_count']=$ok_count;
  }else{
    $hantei="不正解!";
    if(!isset($_SESSION['ok_count'])){
      $ok_count=0;
    }else{  
      $ok_count=$_SESSION['ok_count'];
    }
  }

  // 第何問目
 $next_num = $_POST['ques_num'];

  // dbに保存
 $ques_db = $_SESSION['qcode']; 

 $score = $db->prepare('SELECT max(id) FROM users');
 $score->execute();
 $ques_name = $score->fetch();

 if($ans == $correct){
  $result = 1;
 }else{
  $result = 0; 
 }

 $score2 = $db->prepare('INSERT INTO scores(user_id, quiz_id, answer, is_correct, result) VALUES(?,?,?,?,?)');
 $score2->execute(array(
  $ques_name[0],
  $ques_db, 
  $ans,
  $correct,
  $result
 ));

// カテゴリ名を取得
$cate3 = $_SESSION['cate_id'];

$categories = $db->prepare('SELECT question_name FROM categories WHERE id = :cate');
$categories->bindParam(':cate',$cate3);
$categories->execute();
$cate = ($categories->fetchAll());

?>
<!doctype html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../css/quizzes/_index.css" >
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Exercise</title>
  </head>
  <body>
    <main>
      <div class="container">
        <h4 class="text-center mt-5"><?php echo $cate[0]['question_name'] ?></h4>
          <div class="text-center">(全3問)</div>
          <div class="center-block ques">
              <h2><?php echo $hantei ?></h2>
              <?php
              if($next_num==3):?>
                <form method="post" name="form1" action="result.php">
                  <input type="hidden" name="result_num" value="<?php echo $next_num?>">
                  <input type="hidden" name="result_count" value="<?php echo $ok_count?>">
                  <a class="btn btn-secondary mt-4" href="javascript:form1.submit()">総合結果へ</a>
                </form> 
              <?php else: ?>
                <form method="post" name="form1" action="index.php">
                  <input type="hidden" name="next_num" value="<?php echo $next_num?>">
                  <a class="btn btn-secondary mt-4" href="javascript:form1.submit()">次の問題へ</a>
                </form>
                <p class="mt-4">現在、<?php echo $next_num; ?>問中<?php echo $ok_count;?>問正解しています</p>
              <?php endif ?>  
          </div>
      </div>
    </main>
  </body>    
</html>