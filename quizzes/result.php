<?php 
session_start(); 
require('../dbconnect.php');

$result_num = $_POST['result_num'];
$result_count = $_POST['result_count'];

// カテゴリ名を取得
$cate3 = $_SESSION['cate_id'];

$categories = $db->prepare('SELECT question_name FROM categories WHERE id = :cate');
$categories->bindParam(':cate',$cate3);
$categories->execute();
$cate = ($categories->fetchAll());
?>
<!doctype html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../css/quizzes/_index.css" >
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous"> 
    <title>Exercise</title>
  </head>
  <body>
    <main>
      <div class="container">
        <h4 class="text-center mt-5"><?php echo $cate[0]['question_name'] ?></h4>
          <div class="text-center">(全3問)</div>
            <h4 class="text-center mt-5">
              <?php echo $_SESSION['name'];?> さんの点数は<?php echo $result_count;?>点です
            </h4>
            <div class="col text-center">
              <a class="btn btn-secondary mt-4" href="../categories/index.php" role="button">一覧へ戻る</a>
              <?php session_destroy();?>   
            </div>  
      </div>
    </main>
  </body>    
</html>