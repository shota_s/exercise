<?php
   session_start();
   require('../dbconnect.php');

  //  カテゴリ別問題数・回答者数取得
   $categories2 = $db->prepare("SELECT categories.id, question_name, count(DISTINCT question), count(DISTINCT user_id),round(sum(result)/count(DISTINCT user_id),1) FROM quizzes LEFT JOIN scores on quizzes.id = scores.quiz_id RIGHT JOIN categories on quizzes.category_id = categories.id GROUP BY category_id ORDER BY categories.id");
   $categories2 ->execute();
   $category2 = ($categories2->fetchAll());

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../css/categories/_index.css" >
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Exercise</title>
  </head>
  <body>
  <h2 class="center-block cate-title mt-5">問題一覧</h2>
  <br>
    <div class="container"> 
        <table class="table main-table">
          <thead class="thead-light">
            <tr>
              <th scope="col"class="text-center">問題</th>
              <th scope="col"class="text-center">問題数</th>
              <th scope="col"class="text-center">回答者数</th>
              <th scope="col"class="text-center">平均点</th>
              <th scope="col"class="text-center">統計</th>
              <th scope="col"class="text-center"></th>
            </tr>
          </thead>
          <tbody>
          <?php foreach ($category2 as $cate2) : ?>
            <tr>
              <th scope="row" class="text-center"><?php echo $cate2['question_name'];?></th>
              <td class="text-center"><?php echo $cate2['count(DISTINCT question)'] ;?></td>
              <td class="text-center"><?php echo $cate2['count(DISTINCT user_id)'] ;?></td>
              <td class="text-center"><?php echo $cate2['round(sum(result)/count(DISTINCT user_id),1)'] ;?></td>
              <td class ="statistics text-center">
                <a class="btn-sm btn btn-secondary" href="../scores/index.php?id=<?php echo ($cate2['id']); ?>" role="button">回答者一覧</a>
                <a class="btn-sm btn btn-secondary" href="show.php?id=<?php echo ($cate2['id']); ?>" role="button">問題別回答</a>
              </td>
              <td class="text-center">
                <a class="btn-sm btn btn-secondary" href="../users/input.php?id=<?php echo ($cate2['id']); ?>" role="button">テスト開始</a>
              </td>
            </tr> 
          <?php endforeach; ?>  
          </tbody>
        </table>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>