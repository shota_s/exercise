<?php 
  session_start();
  require('../dbconnect.php');

  // 問題別スコアを取得
  $_SESSION['cate_id'] = $_REQUEST['id'];
  $cate = $_SESSION['cate_id'];

  $questions_score = $db->prepare("SELECT quizzes.id, quizzes.question, count(result = 1 or null), count(result = 0 or null), concat(round(( count(result = 1 or null)/(count(result = 1 or null)+count(result = 0 or null)) * 100 ),1),'%') FROM scores RIGHT JOIN quizzes on scores.quiz_id = quizzes.id LEFT JOIN users on scores.user_id = users.id WHERE quizzes.category_id = :cate GROUP BY quizzes.id ORDER BY quizzes.id");
  $questions_score->bindParam(':cate',$cate);
  $questions_score->execute();
  $question_score = ($questions_score->fetchAll());
  
  $ques_score = array_column($question_score , NULL , 'id');

  // カテゴリ名を取得
  $cate3 = $_SESSION['cate_id'];

  $categories = $db->prepare('SELECT question_name FROM categories WHERE id = :cate');
  $categories->bindParam(':cate',$cate3);
  $categories->execute();
  $cate = ($categories->fetchAll());

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="../css/categories/_index.css" >
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Exercise</title>
  </head>
  <body>
  <h2 class="center-block cate-title mt-5">「<?php echo $cate[0]['question_name'] ?>」の問題別回答</h2>
  <div class="col text-center">
      <a class="btn btn-secondary mt-4" href="../categories/index.php" role="button">一覧へ戻る</a>
  </div>  
    <br>
    <div class="container">
      <table class="table main-table mt-4">
        <thead class="thead-light">
          <tr>
            <th scope="col"class="text-center">No</th>
            <th scope="col"class="text-center">問題</th>
            <th scope="col"class="text-center">正解数</th>
            <th scope="col"class="text-center">誤答数</th>
            <th scope="col"class="text-center">正解率</th>
          </tr>
        </thead>
        <tbody>
        <?php foreach ($ques_score as $q_score) : ?>
          <tr>
            <th scope="row" class="text-center"><?php echo $q_score['id'] ;?></th>
            <td class="text-center"><?php echo $q_score['question'] ;?></td>
            <td class="text-center"><?php echo $q_score['count(result = 1 or null)'] ;?></td>
            <td class="text-center"><?php echo $q_score['count(result = 0 or null)'] ;?></td>
            <td class="text-center">
            <?php echo $q_score["concat(round(( count(result = 1 or null)/(count(result = 1 or null)+count(result = 0 or null)) * 100 ),1),'%')"] ?> 
            </td>
          </tr>
        <?php endforeach; ?>
        </tbody>
      </table>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>